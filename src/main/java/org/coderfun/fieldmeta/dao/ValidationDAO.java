package org.coderfun.fieldmeta.dao;

import klg.j2ee.common.dataaccess.BaseRepository;
import org.coderfun.fieldmeta.entity.Validation;

public interface ValidationDAO extends BaseRepository<Validation, Long> {

}
